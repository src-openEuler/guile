Name:          guile
Version:       2.2.7
Release:       6
Epoch:         5
Summary:       GNU's Ubiquitous Intelligent Language for Extension
License:       LGPLv3+
URL:           https://www.gnu.org/software/guile/
Source:        https://ftp.gnu.org/gnu/guile/%{name}-%{version}.tar.xz

%global g_version 2.2

#Patch from fedora
#https://src.fedoraproject.org/rpms/guile22/blob/f38/f/guile-multilb.patch
Patch1: guile-multilib.patch
#https://src.fedoraproject.org/rpms/guile22/blob/f38/f/guile-threadstest.patch
Patch3: guile-threadstest.patch
#https://src.fedoraproject.org/rpms/guile22/blob/f38/f/disable-out-of-memory-test.patch
Patch4: disable-out-of-memory-test.patch
#https://src.fedoraproject.org/rpms/guile22/blob/f38/f/guile-configure.patch
Patch5: guile-configure.patch
#https://src.fedoraproject.org/rpms/guile/blob/rawhide/f/guile-2.0.14-gc_pkgconfig_private.patch
Patch6000:     guile-2.0.14-gc_pkgconfig_private.patch

BuildRequires: gcc libtool gmp-devel readline-devel gc-devel  libffi-devel
BuildRequires: gettext-devel libunistring-devel  libtool-ltdl-devel
Requires:      coreutils

%description
This is Guile, a portable, embeddable Scheme implementation written in
C. Guile provides a machine independent execution platform that can be
linked in as a library when building extensible programs.

%package       devel
Summary:       Development files and Header files for %{name}
Requires:      guile = %{epoch}:%{version}-%{release} gmp-devel gc-devel pkgconfig 

%description   devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1 

%build
autoreconf -fiv
%configure --disable-error-on-warning
sed -i 's|" $sys_lib_dlsearch_path "|" $sys_lib_dlsearch_path %{_libdir} "|' libtool
%make_build

%install
%make_install
install -d  %{buildroot}%{_datadir}/guile/site/%{g_version}
%delete_la_and_a
rm -f %{buildroot}%{_infodir}/dir
bzip2 NEWS

for i in %{buildroot}%{_infodir}/goops.info; do
    iconv -f iso8859-1 -t utf-8 < $i > $i.utf8 && mv -f ${i}{.utf8,}
done

touch %{buildroot}%{_datadir}/guile/site/%{g_version}/slibcat

ln -s guile %{buildroot}%{_bindir}/guile2
ln -s guile-tools %{buildroot}%{_bindir}/guile-tools2

find %{buildroot}%{_datadir} -name '*.scm' -exec touch -r "%{_specdir}/guile.spec" '{}' \;
find %{buildroot}%{_libdir} -name '*.go' -exec touch -r "%{_specdir}/guile.spec" '{}' \;

%check
make  check || true

%ldconfig_scriptlets

%triggerin -- slib >= 3b4-1
export SCHEME_LIBRARY_PATH=%{_datadir}/slib/

%{_bindir}/guile --fresh-auto-compile --no-auto-compile -c \
    "(use-modules (ice-9 slib)) (require 'new-catalog)" &> /dev/null || \
    rm -f %{_datadir}/guile/site/%{g_version}/slibcat
:
%triggerun -- slib >= 3b4-1
if [ "$2" = 0 ]; then
    rm -f %{_datadir}/guile/site/%{g_version}/slibcat
fi

%files
%doc AUTHORS 
%license COPYING COPYING.LESSER LICENSE
%{_datadir}/guile/%{g_version}/*
%dir %{_datadir}/guile/site/%{g_version}
%{_bindir}/guild
%{_bindir}/guile
%{_bindir}/guile2
%{_bindir}/guile-tools*
%{_libdir}/guile
%{_libdir}/libguile-%{g_version}.so.*
%{_infodir}/guile.info*.gz
%{_infodir}/r5rs.info.gz
%ghost %{_datadir}/guile/site/%{g_version}/slibcat
%exclude %{_libdir}/libguile*gdb.scm

%files devel
%{_bindir}/guile-config
%{_bindir}/guile-snarf
%{_includedir}/guile/%{g_version}/libguile/*.h
%{_includedir}/guile/%{g_version}/*.h
%{_libdir}/pkgconfig/guile-%{g_version}.pc
%{_libdir}/libguile-%{g_version}.so
%{_datadir}/aclocal/guile.m4

%files help
%doc HACKING NEWS.bz2 README THANKS
%{_mandir}/man1/guile.1.gz

%changelog
* Mon Aug 19 2024 zhangpan <zhangpan103@h-partners.com> - 5:2.2.7-6
- backport patch from upstream

* Fri Jul 19 2024 liweigang <liweiganga@uniontech.com> - 5:2.2.7-5
- fix build error(automake 1.17)
- regenerate configure file

* Wed Jan 03 2024 konglidong <konglidong@uniontech.com> - 2.2.7-4
- fix bad macro expansion in changelog

* Sat Apr  8 2023 Z572 <873216071@qq.com> - 2.2.7-3
- fix guile-tools alias 'guile2-tools' -> 'guile-tools2'

* Mon Feb 20 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.2.7-2
- delete old so files

* Mon Feb 13 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.2.7-1
- upgrade to 2.2.7

* Mon Apr 25 2022 yangcheng <yangcheng87@h-partners.com> - 2.0.14-18
- Remove multithreaded builds to resolve binary bep differences

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.0.14-17
- DESC: delete -S git from autosetup, and delete BuildRequires git

* Wed Jul 21 2021 yushaogui <yushaogui@huawei.com> - 2.0.14-16
- delete gdb in buildrequires

* Fri Mar 20 2020 songnannan <songnannan2@huawei.com> - 2.0.14-15
- add gdb in buildrequires

* Sat Sep 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.14-14
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:remove the libguile-2.0.so.22.8.1-gdb.scm from package

* Tue Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.14-13
- Package init
